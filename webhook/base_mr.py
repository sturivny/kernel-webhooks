"""A basic MR class using Graphql."""
from dataclasses import dataclass
from dataclasses import field
from dataclasses import replace
from functools import cached_property
from time import sleep

from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
from gitlab.client import Gitlab
from gitlab.v4.objects.projects import Project
import prometheus_client as prometheus

from webhook import common
from webhook import defs
from webhook import fragments
from webhook.description import Commit
from webhook.description import MRDescription
from webhook.graphql import GitlabGraph
from webhook.rh_metadata import Projects
from webhook.users import User
from webhook.users import UserCache

LOGGER = get_logger('cki.webhook.base_mr')

METRIC_KWF_ZERO_COMMITS = prometheus.Counter(
    'kwf_zero_commits',
    'Number of times commits were found on an MR only after retrying'
)


@dataclass
class BaseMR:
    # pylint: disable=too-many-instance-attributes
    """Hold basic MR data and provide basic functions."""

    MR_QUERY_BASE = """
    query mrData($mr_id: String!, $namespace: ID!, $first: Boolean = true, %(variable_addons)s) {
      ...CurrentUser @include(if: $first)
      project(fullPath: $namespace) {
        id @include(if: $first)
    %(project_addons)s
        mr: mergeRequest(iid: $mr_id) {
          author {
            ...GlUser @include(if: $first)
          }
          description @include(if: $first)
          id @include(if: $first)
          ...MrLabels @include(if: $first)
          state @include(if: $first)
          draft @include(if: $first)
          targetBranch @include(if: $first)
          title @include(if: $first)
    %(mr_addons)s
        }
      }
    }
    """

    MR_QUERY = MR_QUERY_BASE + fragments.CURRENT_USER + fragments.GL_USER + fragments.MR_LABELS

    COMMIT_QUERY_BASE = """
    query mrData($mr_id: String!, $namespace: ID!, $after: String = "") {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...MrCommits
        }
      }
    }
    """

    COMMIT_QUERY = COMMIT_QUERY_BASE + fragments.MR_COMMITS + fragments.GL_USER

    graphql: GitlabGraph
    gl_instance: Gitlab
    projects: Projects
    user_cache: UserCache
    namespace: str
    mr_id: int
    is_dependency: bool = field(default=False)

    # populated by the query response
    author: User | None = field(default=None, init=False)
    current_user: User | None = field(default=None, init=False)
    description: MRDescription = field(default_factory=MRDescription, init=False)
    draft: bool = field(default=False, init=False)
    labels: list = field(default_factory=list, init=False)
    global_id: int = field(default=0, init=False)
    project_id: int = field(default=0, init=False)
    state: defs.MrState = field(default=defs.MrState.UNKNOWN)
    target_branch: str = field(default='', init=False)
    title: str = field(default='', init=False)
    _gl_project: Project | None = field(default=None, init=False)

    '''
    Parameters for the query run on post_init that can be overridden on creation or by a subclass.

    query_string: A valid Graphql query in string format. It can have printf-style conversion
                  specifiers: https://docs.python.org/3/library/stdtypes.html#old-string-formatting
                  Defaults to MR_QUERY.
    query_addons: A dict of conversion specifiers and their values to be applied to query_string.
                  Defaults to {'variable_addons': '', 'project_addons': '', 'mr_addons': ''}.
    query_params_extra: A dict that can extend the default query parameters of namespace and mr_id.
                        Defaults to {}.
    query_func: A function that is passed the query results dict after being passed through
                _parse_base_query().
                Defaults to False.
    '''
    query_string: str = field(default=MR_QUERY, repr=False)
    query_addons: dict = field(repr=False, default_factory=lambda: {'variable_addons': '',
                                                                    'project_addons': '',
                                                                    'mr_addons': ''})
    query_params_extra: dict = field(default_factory=dict, repr=False)
    query_func: callable = field(default=False, repr=False)

    def __repr__(self):
        """Describe yourself."""
        commits = len(self.commits) if 'commits' in self.__dict__ else '???'
        repr_str = f'MR {self.namespace}!{self.mr_id}'
        repr_str += f', commits: {commits}'
        repr_str += f', state: {self.state.name.lower()}'
        repr_str += f', draft: {self.draft}'
        repr_str += f", dependency: {self.is_dependency}"
        repr_str += f", project: {self.project.name if self.project else 'None'}"
        repr_str += f", branch: {self.branch.name if self.branch else 'None'}"
        return f'<{repr_str}>'

    def __post_init__(self):
        """Set up the object by running a query using self.query_* values."""
        query_string = self.query_string if not self.query_addons else \
            self.query_string % self.query_addons
        self.query(query_string, self.query_params_extra, process_function=self._parse_base_query)
        LOGGER.info('Created %s', self)

    @staticmethod
    def __check_query_results(results):
        """Check the query results are useful."""
        if not results:
            LOGGER.warning('No query results??')
            return False
        if not results['project']['mr']:
            LOGGER.warning('Merge request does not exist?')
            return False
        return True

    def _parse_base_query(self, results):
        """Parse the MR_QUERY results into our attributes."""
        self.project_id = int(results['project']['id'].split('/')[-1])
        self.author = self.user_cache.new_user(results['project']['mr']['author'])
        self.current_user = self.user_cache.new_user(results['currentUser'])
        self.description = MRDescription(results['project']['mr']['description'], self.namespace)
        self.draft = results['project']['mr']['draft']
        self.global_id = int(results['project']['mr']['id'].split('/')[-1])
        self.labels = [label['title'] for label in results['project']['mr']['labels']['nodes']]
        self.state = defs.MrState.from_str(results['project']['mr']['state'])
        self.target_branch = results['project']['mr']['targetBranch']
        self.title = results['project']['mr']['title']
        # If self.query_func is set to the name of a function then try to call it.
        if self.query_func:
            self.query_func(results)

    @property
    def all_commits(self):
        """Return a list of all the Commit objects of the MR."""
        if not self.description.text and not self.commits:
            return []
        return [self.faux_description_commit] + list(self.commits.values())

    @property
    def all_descriptions(self):
        """Return a list of all the Description objects of the MR."""
        if not self.description.text and not self.commits:
            return []
        return [self.description] + [commit.description for commit in self.commits.values()]

    @property
    def branch(self):
        """Return the matching Branch object, or None."""
        return self.project.get_branch_by_name(self.target_branch) if self.project else None

    @cached_property
    def faux_description_commit(self):
        """Return a faux Commit derived from the MR Description."""
        # If the MR author has no public email then assume (lol) the first commit is theirs and take
        # its authorEmail?
        author_email = ''
        if self.author and self.author.emails:
            author_email = self.author.emails[0]
        elif self.commits:
            author_email = list(self.commits.values())[0].authorEmail
        author_name = self.author.name if self.author else ''
        return Commit(author=self.author, authorEmail=author_email, authorName=author_name,
                      description=self.description, sha='MR Description', title=self.title)

    @property
    def first_dep_sha(self):
        """Return the sha of the first commit whose Bugzilla tag refers to a Depends."""
        if not self.description or not self.description.depends:
            return ''
        return next((sha for sha, commit in self.commits.items() for bz_id in
                     self.description.depends if bz_id in commit.description.bugzilla), '')

    @property
    def gl_project(self):
        """Return a gl_project object."""
        if not self._gl_project:
            self._gl_project = self.gl_instance.projects.get(self.namespace)
        return self._gl_project

    @property
    def has_internal(self):
        """Return True if the MR description or any commits are marked INTERNAL."""
        return any(description.marked_internal for description in self.all_descriptions)

    @property
    def has_untagged(self):
        """Return True if any commits are UNTAGGED."""
        return not all(commit.description.bugzilla or commit.description.marked_internal for
                       commit in self.commits.values())

    @property
    def project(self):
        """Return the matching Project object, or None."""
        return self.projects.get_project_by_id(self.project_id)

    def query(self, query_string, query_params_extra=None, paged_key=None, process_function=None):
        """Run the given query and process it with the given function."""
        query_params = {'namespace': self.namespace, 'mr_id': str(self.mr_id)}
        if query_params_extra:
            query_params.update(query_params_extra)
        results = self.graphql.client.query(query_string, variable_values=query_params,
                                            paged_key=paged_key)
        if not self.__check_query_results(results):
            return None
        return process_function(results) if process_function else results

    def add_labels(self, to_add):
        """Use common code to add the list of labels to the MR."""
        self.labels[:] = common.add_label_to_merge_request(self.gl_project, self.mr_id, to_add)
        if not set(to_add).issubset(self.labels):
            raise RuntimeError(('The MR does not have the expected labels.'
                                f' Current: {self.labels}, to_add: {to_add}'))

    def remove_labels(self, to_remove):
        """Use common code to remove the list of labels from the MR."""
        self.labels[:] = common.remove_labels_from_merge_request(self.gl_project, self.mr_id,
                                                                 to_remove)
        if not set(to_remove).isdisjoint(self.labels):
            raise RuntimeError(('The MR does not have the expected labels.'
                                f' Current: {self.labels}, to_remove: {to_remove}'))

    @property
    def fresh_commits(self):
        """Return a fresh dict of Commits."""
        if 'commits' in self.__dict__:
            del self.commits
        return self.commits

    @cached_property
    def commits(self):
        """Return a dict of Commit objects with sha as the key."""
        # Ugly hack because maybe the API returned zero commits even though the MR really has some?
        for retry in range(2):
            results = self.query(self.COMMIT_QUERY, paged_key='project/mr/commits')
            if raw_commits := get_nested_key(results, 'project/mr/commits/nodes', []):
                if retry:
                    LOGGER.warning('Only found commits on the second try 🙄.')
                    METRIC_KWF_ZERO_COMMITS.inc()
                break
            if retry:
                LOGGER.info('Still no commits after retry 🤷.')
                break
            LOGGER.info('No commits found, trying again in 10 seconds.')
            sleep(10)
        commits = {}
        for raw_commit in raw_commits:
            # Pass in an author User from the cache.
            author = self.user_cache.new_user(raw_commit.pop('author')) if \
                raw_commit.get('author') else None
            commit = Commit(input_dict=raw_commit, author=author)
            commits[commit.sha] = commit
        return commits

    @cached_property
    def depends_mrs(self):
        # pylint: disable=protected-access
        """Return a list of BaseMR objects representing any Depends: MRs."""
        depends_mrs = []
        depends_mr_ids = self.description.depends_mrs
        loop_limit = 10
        while loop_limit and depends_mr_ids:
            mr_id = depends_mr_ids.pop()
            loop_limit -= 1
            if any(mr for mr in depends_mrs if mr.mr_id == mr_id):
                LOGGER.warning('Dependency MR %s already exists.', mr_id)
                continue
            new_mr = self.new_depends_mr(mr_id)
            depends_mrs.append(new_mr)
            depends_mr_ids.update(new_mr.description.depends_mrs)
            # Any BZ IDs referenced in a dependency MR should be treated as if they were listed in
            # the dependant MR's description.
            self.description._depends.update(new_mr.description.bugzilla |
                                             new_mr.description.depends)
        return depends_mrs

    def new_depends_mr(self, mr_id):
        """Return a new instance of self with dependency=True set."""
        return replace(self, mr_id=mr_id, is_dependency=True)
