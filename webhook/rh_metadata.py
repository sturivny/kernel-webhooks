"""Metadata for RHEL projects."""

from dataclasses import InitVar
from dataclasses import dataclass
from dataclasses import field
from dataclasses import fields
from os import environ
import re

from cki_lib.logger import get_logger
from cki_lib.yaml import load

from webhook.cpc import get_policy_data
from webhook.defs import RH_METADATA_YAML_PATH
from webhook.pipelines import PipelineType

LOGGER = get_logger(__name__)


def check_data(this_dc):
    """Check the dataclass object fields have the right types and are not empty values."""
    dc_type = type(this_dc)
    for dc_field in [field for field in fields(this_dc) if field.init]:
        fvalue = getattr(this_dc, dc_field.name)
        # fields should hold data of the correct type
        if not isinstance(fvalue, dc_field.type):
            raise TypeError(f'{dc_type}.{dc_field.name} must be {dc_field.type}: {this_dc}')
        # do not test for empty values if a field is a bool or has an empty default value
        if isinstance(fvalue, bool) or (isinstance(fvalue, str) and dc_field.default == '') or \
           (isinstance(fvalue, list) and dc_field.default_factory is list):
            continue
        # fields with non-empty default values should not have empty values
        if not fvalue:
            raise ValueError(f'{dc_type}.{dc_field.name} cannot be empty: {this_dc}')


@dataclass(frozen=True, kw_only=True)
class Project:
    """Project metadata."""

    ids: list
    name: str
    product: str
    pipelines: list = field(default_factory=list, repr=False)
    inactive: bool = False
    branches: list = field(default_factory=list, repr=False)

    def __post_init__(self):
        """Set up pipelines and check data."""
        self.__dict__['pipelines'] = [PipelineType.from_str(p) for p in self.pipelines]
        check_data(self)

    def get_branch_by_name(self, branch_name):
        """Return the branch with the given name, or None."""
        return next((b for b in self.branches if b.name == branch_name), None)

    def get_branches_by_itr(self, itr):
        """Return a list of active branches whose policy matches the given ITR."""
        return [b for b in self.branches if b.internal_target_release == itr and not b.inactive]

    def get_branches_by_ztr(self, ztr):
        """Return a list of active branches whose policy matches the given ZTR."""
        return [b for b in self.branches if b.zstream_target_release == ztr and not b.inactive]


@dataclass(frozen=True, eq=True, kw_only=True)
class Branch:
    # pylint: disable=too-many-instance-attributes
    """Branch metadata."""

    name: str
    component: str
    distgit_ref: str
    project: Project = field(compare=False, repr=False)
    sub_component: str = ''
    internal_target_release: str = ''
    zstream_target_release: str = ''
    milestone: str = ''
    inactive: bool = False
    pipelines: list = field(default_factory=list, repr=False)
    policy: list = field(default_factory=list, compare=False, repr=False)

    def __post_init__(self):
        """Set up pipelines and check data."""
        self.__dict__['pipelines'] = [PipelineType.from_str(p) for p in self.pipelines]
        check_data(self)

    @property
    def is_alpha(self):
        """Return True if this is an Alpha Branch."""
        return self.stream == 'Alpha'

    @property
    def is_beta(self):
        """Return True if this is an Beta Branch."""
        return self.stream == 'Beta'

    @property
    def major(self):
        """Return the Major version number."""
        return self._parse_version()[0]

    @property
    def minor(self):
        """Return the Minor version number."""
        return self._parse_version()[1]

    @property
    def stream(self):
        """Return the Stream version number."""
        return self._parse_version()[2]

    def set_policy(self, tokens):
        """Set the given policy."""
        self.__dict__['policy'] = tokens

    def _can_compare(self, other):
        """Do basic comparison checks."""
        if self.component != other.component:
            raise ValueError('Cannot compare Branches from different components.')

    def _parse_version(self):
        """Break up the ITR/ZTR string into its a tuple of its component values."""
        if self.internal_target_release:
            version_string = self.internal_target_release
        elif self.zstream_target_release:
            version_string = self.zstream_target_release
        else:
            return (None, None, None)
        pattern = r'^(?P<major>\d)\.(?P<minor>\d)(?:\.|-)(?P<stream>(?:\d|z|Alpha|Beta))$'
        if result := re.match(pattern, version_string):
            return tuple(int(i) if i.isnumeric() else i for i in result.groups())
        return (None, None, None)

    def __ge__(self, other):
        """Return True if self is greather than or equal to other, otherwise False."""
        self._can_compare(other)
        return self.__gt__(other) or self == other

    def __gt__(self, other):
        """Return True if self is greater than other, otherwise False."""
        self._can_compare(other)
        if other.is_alpha and not self.is_alpha:
            return True
        if other.is_beta and not (self.is_alpha or self.is_beta):
            return True
        return (self.major, self.minor) > (other.major, other.minor)

    def __le__(self, other):
        """Return True if self is less than or equal to other, otherwise False."""
        self._can_compare(other)
        return self.__lt__(other) or self == other

    def __lt__(self, other):
        """Return True if self is less than other, otherwise False."""
        self._can_compare(other)
        if self.is_alpha and not other.is_alpha:
            return True
        if self.is_beta and not (other.is_alpha or other.is_beta):
            return True
        return (self.major, self.minor) < (other.major, other.minor)


@dataclass(frozen=True, kw_only=True)
class Projects:
    """Projects metadata."""

    projects: list = field(default_factory=list, init=False)
    load_policies: InitVar[bool] = False
    yaml_path: InitVar[str] = ''

    def __post_init__(self, load_policies, yaml_path):
        """Load yaml into Projects and check data."""
        if not yaml_path:
            yaml_path = environ.get('RH_METADATA_YAML_PATH', RH_METADATA_YAML_PATH)
        raw_projects = load(file_path=yaml_path)['projects']
        for project in raw_projects:
            # Branches are created with a reference to the Project so we must create the Project
            # first without any Branches.
            branches = project['branches'].copy()
            project['branches'].clear()
            new_project = Project(**project)
            # With a new Project, we can set up the Branches and sneak them in.
            for branch in branches:
                branch['project'] = new_project
                # If the Branch has no pipelines then copy the value from the project
                if not branch.get('pipelines'):
                    branch['pipelines'] = project.get('pipelines', [])
                # If the Project is inactive then mark all the Branches inactive
                if new_project.inactive:
                    branch['inactive'] = True
            new_project.__dict__['branches'] = [Branch(**b) for b in branches]
            self.__dict__['projects'].append(new_project)
        if load_policies:
            self.do_load_policies()
        check_data(self)

    @staticmethod
    def _format_project_id(project_id):
        """Return the project_id as an int."""
        # project_id can be a number or a Gitlab ID scalar type, ie. gid://gitlab/Project/1234567
        if isinstance(project_id, str) and project_id.startswith('gid://gitlab/Project/'):
            project_id = project_id.removeprefix('gid://gitlab/Project/')
        return int(project_id)

    def get_project_by_id(self, project_id):
        """Return the project with the given project_id, or None."""
        project_id = self._format_project_id(project_id)
        return next((p for p in self.projects if project_id in p.ids), None)

    def get_project_by_name(self, project_name):
        """Return the project with the given name, or None."""
        return next((p for p in self.projects if p.name == project_name), None)

    def get_target_branch(self, project_id, target_branch):
        """Return the Branch object matching the given project_id and target_branch."""
        project_id = self._format_project_id(project_id)
        return next((b for p in self.projects for b in p.branches if
                     project_id in p.ids and b.name == target_branch), None)

    def do_load_policies(self):
        """Load policy tokens for each branch."""
        policies = get_policy_data(policy_regex=r'^(rhel-.*|c\d+s)$')
        for project in self.projects:
            for branch in project.branches:
                if policy := policies.get(branch.distgit_ref):
                    branch.set_policy(policy)


def is_branch_active(projects, project_id, target_branch):
    """Return True if the target branch is active, otherwise False."""
    if isinstance(project_id, int) or project_id.isdigit():
        project = projects.get_project_by_id(project_id)
    else:
        project = projects.get_project_by_name(project_id)
    if project:
        if branch := project.get_branch_by_name(target_branch):
            return not branch.inactive
    else:
        LOGGER.warning('Project %s is not present in rh_metadata.', project_id)
    return False
