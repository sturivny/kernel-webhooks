"""Test sprinter webhook."""
import json
import unittest
from unittest import mock

from cki_lib import config_tree
import gitlab
import responses

from webhook import sprinter


class TestSprinterHelpers(unittest.TestCase):
    """Test sprinter webhook helpers."""

    def test_get_label_name(self):
        """Test get_label_name function."""
        cases = [
            # First monday of year: 04/01
            ('2021-01-04T00:00:00.000Z', 'CWF::Sprint::2021-week-1'),
            # First monday of year: 03/01
            ('2022-01-03T00:00:00.000Z', 'CWF::Sprint::2022-week-1'),
            ('2022-01-10T00:00:00.000Z', 'CWF::Sprint::2022-week-2'),
            ('2022-02-02T00:00:00.000Z', 'CWF::Sprint::2022-week-5'),
        ]

        for date, expected in cases:
            with self.subTest(date=date):
                self.assertEqual(sprinter.get_label_name(date), expected)

    def test_run_issue_commands(self):
        """Test run_issue_commands function."""
        cases = (
            (True, ['/foo', '/bar'], json.dumps({'body': '/foo\n/bar'})),
            (False, ['/foo', '/bar'], None),
        )
        for is_production, cmds, body in cases:
            with self.subTest(is_production=is_production), \
                    responses.RequestsMock() as rsps, \
                    mock.patch('cki_lib.misc.is_production', return_value=is_production):
                rsps.get('https://instance/api/v4/projects/g%2Fp',
                         json={'id': 1, 'web_url': 'https://instance/g/p'})
                if body:
                    rsps.post('https://instance/api/v4/projects/1/issues/1/notes', json={})

                gl_project = gitlab.Gitlab('https://instance').projects.get('g/p')
                sprinter.run_issue_commands(gl_project, 1, cmds)
                if body:
                    self.assertEqual(rsps.calls[-1].request.body, body.encode('utf8'))

    @mock.patch('webhook.sprinter.common.generic_loop')
    def test_main(self, mock_loop):
        """Test main entrypoint."""
        sprinter.main({})
        mock_loop.assert_called_with(mock.ANY, sprinter.WEBHOOKS)
        self.assertTrue(mock_loop.call_args[0][0].disable_inactive_branch_check)
        self.assertTrue(mock_loop.call_args[0][0].disable_closed_status_check)
        self.assertEqual(sprinter.WEBHOOKS, {
            'issue': sprinter.process_issue,
            'merge_request': sprinter.process_mr,
        })

    def test_enabled_for_project(self):
        """Test rule checking."""
        cases = (
            ([], {'enabled': True}),
            ([{}], {'enabled': True}),
            ([{'enabled': False, 'project': 'h/group/project'}], None),
            ([{'enabled': False, 'project': 'h/group/project-other'}], {'enabled': True}),
            ([{'enabled': False, 'group': 'h/group'}], None),
            ([{'enabled': False, 'group': 'h/group-other'}], {'enabled': True}),
            ([{'enabled': False, 'group': 'h/grou'}], {'enabled': True}),
            ([{'enabled': False}], None),
            ([{'enabled': True, 'foo': 'bar'}], {'enabled': True, 'foo': 'bar'}),
        )
        for rules, expected in cases:
            with self.subTest(rules=rules):
                self.assertEqual(sprinter.enabled_for_project(rules, 'h/group/project'), expected)


@unittest.mock.patch('webhook.sprinter.check_incident_labels', mock.Mock())
class TestProcessSprintLabels(unittest.TestCase):
    """Test sprinter webhook handling of sprint labels."""

    def setUp(self):
        """Set up common mocks."""
        responses.get('https://instance/api/v4/groups/g',
                      json={'id': 3, 'path': 'g'})
        responses.get('https://instance/api/v4/projects/g%2Fp',
                      json={'path_with_namespace': 'g/p', 'id': 1, 'namespace': {'full_path': 'g'}})
        responses.get('https://instance/api/v4/projects/1/labels',
                      json=[])

    @responses.activate
    def test_sprint_labels(self):
        """Check sprint relabeling."""
        cases = (
            ({
                'object_attributes': {
                    'action': 'open',
                    'labels': []
                }
            }, None),
            ({
                'object_attributes': {
                    'action': 'merge',
                    'labels': []
                }
            }, None),
            ({
                'object_attributes': {
                    'action': 'foobar',
                    'labels': []
                }
            }, None),
            ({
                'object_attributes': {
                    'action': 'close',
                    'closed_at': '2022-01-07 16:11:56 UTC',
                    'labels': [
                        {'title': 'foo'},
                        {'title': 'bar'},
                    ]
                }
            }, ['/label "CWF::Sprint::2022-week-1"']),
            ({
                'object_attributes': {
                    'action': 'reopen',
                    'labels': [
                        {'title': 'foo'},
                        {'title': 'bar'},
                        {'title': 'CWF::Sprint::2022-week-1'}
                    ]
                }
            }, ['/unlabel "CWF::Sprint::2022-week-1"']),
        )

        for config, cmds in cases:
            with self.subTest(config=config), \
                    unittest.mock.patch('webhook.sprinter.run_issue_commands') as commands:
                sprinter.process_issue(None, mock.Mock(payload=config_tree.merge_dicts(config, {
                    'changes': {},
                    'project': {'web_url': 'https://instance/g/p'},
                    'object_attributes': {'iid': 2},
                })))
                if cmds:
                    commands.assert_called_with(unittest.mock.ANY, unittest.mock.ANY, cmds)
                else:
                    commands.assert_not_called()


class TestProcessMR(unittest.TestCase):
    """Test sprinter webhook."""

    def setUp(self):
        """Set up common mocks."""
        responses.get('https://instance/api/v4/projects/g%2Fp',
                      json={'path_with_namespace': 'g/p', 'id': 1, 'namespace': {'full_path': 'g'}})
        responses.get('https://instance/api/v4/groups/g',
                      json={'id': 3, 'path': 'g'})
        responses.get('https://instance/api/v4/projects/1/merge_requests/2',
                      json={'iid': 2, 'labels': [], 'target_branch': 'main', 'draft': False})

    @responses.activate
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_issue_links(self, mock_add_label):
        """Check issue links."""
        cases = (
            ('open without ref', {
                'changes': [],
                'object_attributes': {
                    'action': 'open',
                    'description': 'foo',
                },
            }, {}, 'CWF::Issue::Missing', None),
            ('open with ref', {
                'changes': [],
                'object_attributes': {
                    'action': 'open',
                    'description': '#1',
                },
            }, {},  'CWF::Issue::OK', None),
            ('description update without ref', {
                'changes': ['description'],
                'object_attributes': {
                    'action': 'update',
                    'description': 'foo',
                },
            }, {}, 'CWF::Issue::Missing', None),
            ('description update with ref', {
                'changes': ['description'],
                'object_attributes': {
                    'action': 'update',
                    'description': '#1',
                },
            }, {}, 'CWF::Issue::OK', None),
            ('description update with long ref', {
                'changes': ['description'],
                'object_attributes': {
                    'action': 'update',
                    'description': 'cki-project/cki-lib#1',
                },
            }, {}, 'CWF::Issue::OK', None),
            ('description update with url ref', {
                'changes': ['description'],
                'object_attributes': {
                    'action': 'update',
                    'description': 'https://gitlab.com/cki-project/cki-lib#1',
                },
            }, {},  'CWF::Issue::OK', None),
            ('description update with issue url', {
                'changes': ['description'],
                'object_attributes': {
                    'action': 'update',
                    'description': 'https://gitlab.com/cki-project/cki-lib/-/issues/1',
                },
            }, {},  'CWF::Issue::OK', None),
            ('no description change', {
                'changes': [],
                'object_attributes': {
                    'description': 'foo',
                },
            }, {},  None, None),
            ('project level labels', {
                'changes': [],
                'object_attributes': {
                    'action': 'open',
                    'description': 'foo',
                },
            }, {
                'issue_checks': [{'level': 'project'}]
            }, 'CWF::Issue::Missing', 'project'),
        )
        for description, payload, config, label, level in cases:
            mock_add_label.reset_mock()
            with (self.subTest(description=description),
                  mock.patch('webhook.sprinter.CONFIG', config),
                  mock.patch('webhook.common.add_label_to_merge_request') as mock_add_label):
                sprinter.process_mr(None, mock.Mock(payload=config_tree.merge_dicts(payload, {
                    'project': {'web_url': 'https://instance/g/p'},
                    'object_attributes': {'iid': 2},
                })))

                if label:
                    mock_add_label.assert_called_once_with(mock.ANY, 2, [label], level=level)
                else:
                    mock_add_label.assert_not_called()


@unittest.mock.patch('webhook.sprinter.check_sprint_label', mock.Mock())
class TestProcessIncidents(unittest.TestCase):
    """Test sprinter webhook handling of incident labels."""

    def setUp(self):
        """Set up common mocks."""
        responses.get('https://instance/api/v4/groups/g',
                      json={'id': 3, 'path': 'g'})
        responses.get('https://instance/api/v4/projects/g%2Fp',
                      json={'path_with_namespace': 'g/p', 'id': 1, 'namespace': {'full_path': 'g'}})
        responses.get('https://instance/api/v4/projects/1/labels',
                      json=[])

    @responses.activate
    def test_incident_labels(self):
        """Check incident relabeling."""
        cases = (
            ({
                'object_attributes': {
                    'action': 'open',
                    'labels': [{'title': 'foo'}],
                },
            }, None),
            ({
                'object_attributes': {
                    'action': 'open',
                    'labels': [{'title': 'CWF::Type::Incident'}],
                },
            }, ['/label "CWF::Incident::Active"']),
            ({
                'object_attributes': {
                    'action': 'open',
                    'labels': [
                        {'title': 'CWF::Incident::Active'},
                    ],
                },
            }, ['/label "CWF::Type::Incident"']),
            ({
                'object_attributes': {
                    'action': 'open',
                    'labels': [
                        {'title': 'CWF::Incident::Active'},
                        {'title': 'CWF::Type::Incident'},
                    ],
                },
            }, None),
            ({
                'object_attributes': {
                    'action': 'reopen',
                    'labels': [{'title': 'CWF::Type::Incident'}],
                },
            }, ['/label "CWF::Incident::Active"']),
            ({
                'object_attributes': {
                    'state': 'opened',
                    'action': 'update',
                    'labels': [{'title': 'CWF::Type::Incident'}],
                },
            }, ['/label "CWF::Incident::Active"']),
            ({
                'object_attributes': {
                    'state': 'opened',
                    'action': 'update',
                    'labels': [{'title': 'CWF::Incident::Active'}],
                },
            }, ['/label "CWF::Type::Incident"']),
            ({
                'object_attributes': {
                    'state': 'opened',
                    'action': 'update',
                    'labels': [{'title': 'CWF::Incident::Active'}],
                },
                'changes': {'labels': {'previous': [{'title': 'CWF::Type::Incident'}]}},
            }, ['/unlabel "CWF::Incident::Active"']),
            ({
                'object_attributes': {
                    'state': 'closed',
                    'action': 'update',
                    'labels': [{'title': 'CWF::Type::Incident'}],
                },
            }, None),
            ({
                'object_attributes': {
                    'action': 'close',
                    'labels': [
                        {'title': 'CWF::Incident::Active'},
                        {'title': 'CWF::Type::Incident'},
                    ],
                },
            }, ['/unlabel "CWF::Incident::Active"']),
            ({
                'object_attributes': {
                    'action': 'close',
                    'labels': [
                        {'title': 'CWF::Type::Incident'},
                    ],
                },
            }, None),
        )
        for config, cmds in cases:
            with self.subTest(config=config), \
                    unittest.mock.patch('webhook.sprinter.run_issue_commands') as commands:
                sprinter.process_issue(None, mock.Mock(payload=config_tree.merge_dicts(config, {
                    'changes': {},
                    'project': {'web_url': 'https://instance/g/p'},
                    'object_attributes': {'iid': 2},
                })))
                if cmds:
                    commands.assert_called_with(unittest.mock.ANY, unittest.mock.ANY, cmds)
                else:
                    commands.assert_not_called()
